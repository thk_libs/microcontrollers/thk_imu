/*
 * Klasse: IMU
 *
 * Eine Klasse für die leichte Initializierung und Ansteuerung des Beschleunigungssensors MPU6050.
 * 
 * O.-T. Altan, Januar 2022
 * 
 */

#ifndef thk_IMU_H
#define thk_IMU_H

/* 
* Debugging Mode
*/
#ifdef DEBUG_IMU
#define _println_imu_(x) Serial.println(x)
#define _print_imu_(x) Serial.print(x)
#else
#define _println_imu_(x)
#define _print_imu_(x)
#endif

#include <Arduino.h>
#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"
#include "Wire.h"

/* *************************** *
 * Interrupt Detection Routine *
 * *************************** */
volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}

class thk_IMU
{       
    public:
        thk_IMU(const int INTERRUPT_PIN) : INTERRUPT_PIN(INTERRUPT_PIN){}

        void init()
        {
            // initialize device
            _println_imu_(F("Initializing I2C devices..."));
            mpu.initialize();

            if(INTERRUPT_PIN != 0){
                pinMode(INTERRUPT_PIN, INPUT);
            }
 
            // verify connection
            _println_imu_(F("Testing device connections..."));
            _println_imu_(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

            // load and configure the DMP
            _println_imu_(F("Initializing DMP..."));
            devStatus = mpu.dmpInitialize();

            // supply your own gyro offsets here, scaled for min sensitivity
            mpu.setXGyroOffset(88);
            mpu.setYGyroOffset(-5);
            mpu.setZGyroOffset(3);
            mpu.setZAccelOffset(1528);

            // make sure it worked (returns 0 if so)
            if (devStatus == 0)
            {
                // Calibration Time: generate offsets and calibrate our MPU6050
                mpu.CalibrateAccel(6);
                mpu.CalibrateGyro(6);
                mpu.PrintActiveOffsets();
                // turn on the DMP, now that it's ready
                _println_imu_(F("Enabling DMP..."));
                mpu.setDMPEnabled(true);

                if(INTERRUPT_PIN != 0){
                    // enable Arduino interrupt detection
                    _print_imu_(F("Enabling interrupt detection (Arduino external interrupt "));
                    _print_imu_(digitalPinToInterrupt(INTERRUPT_PIN));
                    _println_imu_(F(")..."));
                    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
                    mpuIntStatus = mpu.getIntStatus();
                }
                // set our DMP Ready flag so the main loop() function knows it's okay to use it
                _println_imu_(F("DMP/IMU ready! Waiting for first interrupt..."));
                dmpReady = true;

                // get expected DMP packet size for later comparison
                packetSize = mpu.dmpGetFIFOPacketSize();
            }
            else
            {
                // ERROR!
                // 1 = initial memory load failed
                // 2 = DMP configuration updates failed
                // (if it's going to break, usually the code will be 1)
                _print_imu_(F("DMP Initialization failed (code "));
                _print_imu_(devStatus);
                _println_imu_(F(")"));
            }
        };

        void reset()
        {
            mpu.initialize();
            
            // load and configure the DMP
            _println_imu_(F("Initializing DMP..."));
            devStatus = mpu.dmpInitialize();

            // make sure it worked (returns 0 if so)
            if (devStatus == 0)
            {
                // Calibration Time: generate offsets and calibrate our MPU6050
                mpu.CalibrateAccel(6);
                mpu.CalibrateGyro(6);
                // turn on the DMP, now that it's ready
                _println_imu_(F("Enabling DMP..."));
                mpu.setDMPEnabled(true);

                // set our DMP Ready flag so the main loop() function knows it's okay to use it
                _println_imu_(F("DMP/IMU ready! Waiting for first interrupt..."));
                dmpReady = true;

                // get expected DMP packet size for later comparison
                packetSize = mpu.dmpGetFIFOPacketSize();
            }
            else
            {
                // ERROR!
                // 1 = initial memory load failed
                // 2 = DMP configuration updates failed
                // (if it's going to break, usually the code will be 1)
                _print_imu_(F("DMP Initialization failed (code "));
                _print_imu_(devStatus);
                _println_imu_(F(")"));
            }
        };

        void update_values()
        {
            if (!dmpReady)
                return;
            if (mpu.dmpGetCurrentFIFOPacket(fifoBuffer))
            { // Get the Latest packet
                    mpu.dmpGetQuaternion(&quat, fifoBuffer);
                    mpu.dmpGetGravity(&gravity, &quat);
                    mpu.dmpGetYawPitchRoll(ypr, &quat, &gravity);
            }
        };   

        float get_quat_w()
        {
            update_values();
            return quat.w;
        };

        float get_quat_x()
        {
            update_values();
            return quat.x;
        };

        float get_quat_y()
        {
            update_values();
            return quat.y;
        };

        float get_quat_z()
        {
            update_values();
            return quat.z;
        };

        float get_z_rot()
        {
            update_values();
            return ypr[0] * 180 / M_PI;
        };    
        
        float get_x_rot()
        {
            update_values();
            return ypr[1] * 180 / M_PI;
        };

        float get_y_rot()
        {
            update_values();
            return ypr[2] * 180 / M_PI;
        };

        float get_orientation()
        {
            update_values();
            return (ypr[0] * 180 / M_PI), (ypr[1] * 180 / M_PI), (ypr[2] * 180 / M_PI);
        }

    private:
        MPU6050 mpu;
        const int INTERRUPT_PIN;
        // MPU control/status vars
        bool dmpReady = false;  // set true if DMP init was successful
        uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
        uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
        uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
        uint16_t fifoCount;     // count of all bytes currently in FIFO
        uint8_t fifoBuffer[64]; // FIFO storage buffer

        // orientation/motion vars
        Quaternion quat;     // [w, x, y, z]         quaternion container
        VectorInt16 aa;      // [x, y, z]            accel sensor measurements
        VectorInt16 aaReal;  // [x, y, z]            gravity-free accel sensor measurements
        VectorInt16 aaWorld; // [x, y, z]            world-frame accel sensor measurements
        VectorFloat gravity; // [x, y, z]            gravity vector
        float euler[3];      // [psi, theta, phi]    Euler angle container
        float ypr[3];        // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
};

#endif
