//#define DEBUG_IMU
#include <thk_imu.h>

long start;
float z_rot, x_rot, y_rot;
float quat_w, quat_x, quat_y, quat_z;

const int INTERRUPT_PIN = 0;   // Wenn kein Interrupt Pin angeschlossen ist, dann Interrupt Pin gleich 0 setzen.
thk_IMU imu(INTERRUPT_PIN);

void setup(){
    Serial.begin(115200);
    Wire.begin();
    imu.init();
    start = millis();
}

void loop(){
    // Auslesen der Yaw, Pitch und Roll Winkel einzeln
    z_rot = imu.get_z_rot();
    x_rot = imu.get_x_rot();
    y_rot = imu.get_y_rot();
    // z_rot, x_rot, y_rot = imu.get_orientation();    // Alternative Methode zum auslesen der Yaw, Pitch und Roll Winkel gemeinsam

    Serial.print(z_rot);
    Serial.print("\t");
    Serial.print(x_rot);
    Serial.print("\t");
    Serial.print(y_rot);
    Serial.print("\t");
    
    // Auslesen der Quarternionen
    quat_w = imu.get_quat_w();
    quat_x = imu.get_quat_x();
    quat_y = imu.get_quat_y();
    quat_z = imu.get_quat_z();
    Serial.print(quat_w);
    Serial.print("\t");
    Serial.print(quat_x);
    Serial.print("\t");
    Serial.print(quat_y);
    Serial.print("\t");
    Serial.print(quat_z);
    Serial.println("\t");

    // Nach 10 Sekunden einen Reset der IMU durchführen
    if ((millis()-start) > 10000){
        imu.reset();
        Serial.println("###############");
        Serial.println("Imu reset done!");
        start = millis();
    }
}